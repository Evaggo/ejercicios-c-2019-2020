#include <stdio.h>

//06. Write a program that asks for a value in centimeter and convert it into meter and kilometer.

int main ()
{
    float cm;
    float tmp;
    
    //Informa de lo que hace el programa
    printf("Si me dices una longitud en centimetros, te la paso a metros y kilometros\n");
    scanf("%f", &cm); //Pide los centimetros
    getchar();
    
    //Limpia la pantalla
    system("cls");
    
    //Transforma a metros
    tmp = cm / 100;
    printf("El equivalente en metros es: %f\n", tmp);
    
    //Transforma a quilometros
    tmp = cm / 100000;
    printf("El equivalente en quilometros es: %f\n", tmp);

    
    getchar();
    return 0;
}