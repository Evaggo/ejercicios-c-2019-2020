#include <stdio.h>

int main ()
{

    int division = 0;
    int cont = 0;
    int i;
    
    printf("A continuacion te doy los primeros 100 numeros primos:");
    getchar();
    
    //Para generar los numeros primos 
    for (int primos = 2; cont < 100; primos++)
    {
        division = 0;
        i = 2;
            
        //Para comprovar si es primo o no
        while (i <= primos && division == 0)
        {
            if (primos%i == 0 && primos==i) //Si es primo lo escribe
            {
                printf (" %d \n", primos);
                cont++;
                
            } else if (primos%i == 0) { //Si no lo es pasa de escribirlo
                division = 1;
            }
            
                i++;
                
            }
             
        }
    
  
    getchar();
    return 0;
}