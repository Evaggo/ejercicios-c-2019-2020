#include <stdio.h>
#include <stdlib.h>

//Crea la funcion para saber si es negativo o no
int IsNegative (int num){
    if (num < 0){
        printf("Es negativo");
        return 0;
    } else {
        printf("Es positivo");
        return 1;
    }
}

int main ()
{
    int numero; 
    int comprobar;
    
    //Informa de lo que hace el programa
    printf("Dame un numero y te dire si es negativo o positivo\n");
    
    //Pide un numero
    scanf("%d", &numero);
    getchar();
    
    //Le da un valor a la funcion IsNegative para que compruebe si es negativo o no 
    comprobar = IsNegative(numero);    
    
 
   getchar();
   return 0;
}