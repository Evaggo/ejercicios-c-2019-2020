#include <stdio.h>
#include <stdlib.h>

int RaiseToPower(int value, int power){
    int mult = 1;
    for (int i; i < power; i++){
        mult *= value;
    }
    return mult;
}

int main ()
{
    int num;
    int potencia;
    int resultado;
    
    //Informa de lo que hace el programa
    printf("Dime un numero y su potencia y te lo calculo!\n");
    getchar();
    system("cls"); //Limpia la pantalla
    
    //Pide el numero y la potencia
    printf("Dame un numero: \n");
    scanf("%d", &num);
    getchar();
    printf("Dime a que lo quieres elevar: \n");
    scanf("%d", &potencia);
    getchar();
    
    system("cls"); //Limpia la pantalla
    
    //Usa la funcion para calcular el resultado
    resultado = RaiseToPower(num, potencia);
    
    printf("%d elevado a %d es igual a %d", num, potencia, resultado);
    
   getchar();
   return 0;
}