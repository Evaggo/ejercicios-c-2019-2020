#include <stdio.h>

//05. Write a program that asks for radius of a circle and find its diameter, circumference and area.

int main ()
{
    float radio;
    float tmp;
    
    //Informa de lo que hace el programa
    printf("Si me dices el radio de una circumferencia, te digo el diametro, \nla longitud de la circumferencia y el area que ocupa \n");
    scanf("%f", &radio); //Pide el radio
    getchar();
    
    //Limpia la pantalla
    system("cls");
    
    //Calcula el diametro 
    tmp = radio*2;
    printf("El diametro de la circumferencia es: %f\n", tmp);
    
    //Calcula la longitd de la circumferencia
    tmp = 3.14159*radio*2;
    printf("La longitud de la circumferencia es: %f\n", tmp);
    
    //Calcula el area
    tmp = 3.14159*radio*radio;
    printf("El area que ocupa la circumferencia es: %f\n", tmp);
    
    
    getchar();
    return 0;
}