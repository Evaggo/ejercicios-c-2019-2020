#include <stdio.h>

//12. Write a program that asks for 10 numbers to the user and show the sum and product of all of them. Use while loop.

int main ()
{
    int num;
    int tmp = 0;
    int sumatotal = 0;
    int multiplicaciontotal = 1;
    
    //Informa de lo que va a hacer el programa
    printf("Si me das 10 numeros, te los sumo y te los multiplico");
    getchar();
    system("cls"); //Limpia la pantalla
    
    while (tmp < 10) {
        
        //Pide un numero
        printf("Dame un numero: "); 
        scanf("%i", &num);
        getchar();
        system("cls"); //Limpia la pantalla
        
        tmp++;
        sumatotal += num; //Suma el numero
        multiplicaciontotal *= num; //Multiplica el numero
    }
    
    //Informa de los resultados
    printf("La suma total es: %d\n", sumatotal);
    printf("La multiplicacion total es: %d\n", multiplicaciontotal);
       
    getchar();
    return 0;
}