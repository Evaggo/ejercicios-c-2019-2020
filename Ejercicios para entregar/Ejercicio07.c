#include <stdio.h>

//07. Write a program that asks for a temperature in Celsius(°C) and convert it into Fahrenheit(°F).

int main ()
{
    float grados;
    float tmp;
    
    //Informa de lo que hace el programa
    printf("Si me dices una temperatura en grados centigrados, te doy su equivalencia en Farenheit\n");
    scanf("%f", &grados); //Pide el los grados centigrados
    getchar();
    
    //Limpia la pantalla
    system("cls");
    
    //Pasa de ºC a ºF
    tmp =  (grados * 1.8) + 32;
    printf("El equivalente es: %f\n", tmp);
    


    
    getchar();
    return 0;
}