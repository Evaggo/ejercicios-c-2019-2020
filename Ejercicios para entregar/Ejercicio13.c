#include <stdio.h>

//13. Write a program that asks for a sequence of numbers to the user and show the sum of all of them. Process stops when a negative number is introduced.

int main ()
{
    int num;
    int sumatotal = 0;
    
    //Informa de lo que va a hacer el programa
    printf("Te sumare todos los numeros que me des hasta que me des un numero negativo o 0");
    getchar();
    system("cls"); //Limpia la pantalla
    
    while (num > 0) {
        
        //Pide un numero
        printf("Dame un numero: "); 
        scanf("%i", &num);
        getchar();
        system("cls"); //Limpia la pantalla
        
        sumatotal += num; //Suma el numero
        
    }
    
    //Informa de los resultados
    printf("La suma total es: %d\n", sumatotal);
    
    
       
    getchar();
    return 0;
}