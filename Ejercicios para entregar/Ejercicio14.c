#include <stdio.h>

//14. Write a program that asks for a sequence of numbers to the user and show the mean of all of them. Process stops only when number 0 is introduced.

int main ()
{
    int num;
    int sumatotal = 0;
    float mean;
    int recuento = 0;
    
    //Informa de lo que va a hacer el programa
    printf("Te hare la media de todos los numeros que me des hasta que me des el 0");
    getchar();
    system("cls"); //Limpia la pantalla
    
    while (num != 0) {
        
        //Pide un numero
        printf("Dame un numero: "); 
        scanf("%i", &num);
        getchar();
        system("cls"); //Limpia la pantalla
        
        sumatotal += num; //Suma el numero
        recuento++;
        
    }
    
    //Calcula la media
    mean = sumatotal / recuento;
    
    //Informa de los resultados
    printf("La media es: %f\n", mean);
    
    
       
    getchar();
    return 0;
}