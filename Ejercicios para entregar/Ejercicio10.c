#include <stdio.h>

//10. Write a program that asks for 2 angles of a triangle and find the third angle.

int main ()
{
    float angle1;
    float angle2;
    float angle3;
    
    //Informa de lo que hace el programa
    printf("Si me dices dos angulos de un triangulo, te digo el angulo que falta\n");
    printf("Angulo 1: ");
    scanf("%f", &angle1); //Pide el primer angulo
    
    //Comprueba que el primer angulo sea un angulo aceptable
   if (angle1 > 0 && angle1 < 180) {
        getchar();
        printf("Angulo 2: ");
        scanf("%f", &angle2); //Pide el segundo angulo
        getchar();
        
        //Limpia la pantalla
            system("cls");
        
        //Si el angulo1 es decente, comprueba que el segundo angulo sea un angulo aceptable
        if (angle2 > 0 && angle2 < 180) {
            
            //Calcula el tercer angulo
            angle3 = 180 - angle1 - angle2; //180 = angulos totales de un triangulo
            
            //Si el angulo2 tambien es decente, comprueba que el angulo total sea un numero positivo y diferente a 0
            if (angle3 <= 0) { //Como es negativo o igual a 0,los datos introducidos por el usuario han ido a trolear
                printf("Me has timado en algun dato. Los angulos que me has dado superan los 180 grados de un triangulo. \n");
            } else { //Si es diferente a 0 y positivo, se muestra el angulo
                printf("El tercer angulo es: %f\n", angle3);
            }
            
        } else { //Si el angulo2 no es un numero decente:
            printf("Un angulo no puede ser menor que 0 ni mayor que 180\n");
            printf("asi que deja de tomarme el pelo y pirate de aqui, pringao\n");
        }
        
    } else { //Si el angulo1 no es un numero decente:
         printf("Un angulo no puede ser menor que 0 ni mayor que 180\n");
          printf("asi que deja de tomarme el pelo y pirate de aqui, pringao\n");
    }
   
    getchar();
    return 0;
}