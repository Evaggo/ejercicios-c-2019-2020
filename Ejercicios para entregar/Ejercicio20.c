#include <stdio.h>
#include <stdlib.h>

int main ()
{
    float num1;
    float num2;
    char operacion;
    float resultado;
    
    //Informa de lo que hace el programa
    printf("Soy un intento de calculadora");
    getchar();
    system("cls"); //Limpia la pantalla
    
    
    
    while (operacion != 'n') {
        
        //Pide el primer numero
        printf("Primer numero: \n");
        scanf("%f", &num1);
        getchar();
        
        //Pide la operacion a realizar
        printf("Que operacion quieres realizar: \n");
        printf("+ para sumar \n- para restar \n* para multiplicar \n/ para dividir \n");
        scanf("%c", &operacion);
        getchar();
        
        if (operacion == '+') {
            
            //Pide el segundo numero
            printf("Segundo numero: \n");
            scanf("%f", &num2);
            getchar();
            resultado = num1 + num2;
            
        } else if (operacion == '-') {
            
            //Pide el segundo numero
            printf("Segundo numero: \n");
            scanf("%f", &num2);
            getchar();
            resultado = num1 - num2;
            
        } else if (operacion == '*') {
            
            //Pide el segundo numero
            printf("Segundo numero: \n");
            scanf("%f", &num2);
            getchar();
            resultado = num1 * num2;
            
        } else if (operacion == '/') {
            
            //Pide el segundo numero
            printf("Segundo numero: \n");
            scanf("%f", &num2);
            getchar();
            resultado = num1 / num2;
            
        } else {
            
            printf("Eso no era ni una opción");
            
        }
        
        printf("La respuesta es %f", resultado);
        getchar();
        
        system("cls");
        
        printf("Deseas continuar usando la calculadora?\n");
        printf("y = si\nn = no\n");
        scanf("%c", &operacion);
        getchar();
        system("cls");
                
    }
    
    printf("Hasta otra");
    
   
    /*
    
    //Pide la operacion a realizar
    printf("Que operacion quieres realizar: \n");
    printf("+ para sumar \n- para restar \n* para multiplicar \n/ para dividir \n");
    scanf("%c", &operacion);
    getchar();
    
    if (operacion == '+') {
        
        //Pide el segundo numero
        printf("Segundo numero: \n");
        scanf("%f", &num2);
        getchar();
        resultado = num1 + num2;
        
    } else if (operacion == '-') {
        
        //Pide el segundo numero
        printf("Segundo numero: \n");
        scanf("%f", &num2);
        getchar();
        resultado = num1 - num2;
        
    } else if (operacion == '*') {
        
        //Pide el segundo numero
        printf("Segundo numero: \n");
        scanf("%f", &num2);
        getchar();
        resultado = num1 * num2;
        
    } else if (operacion == '/') {
        
        //Pide el segundo numero
        printf("Segundo numero: \n");
        scanf("%f", &num2);
        getchar();
        resultado = num1 / num2;
        
    } else {
        
        printf("Eso no era ni una opción");
        
    }
    
    printf("La respuesta es %f", resultado);   
    
    */

   getchar();
   return 0;
}