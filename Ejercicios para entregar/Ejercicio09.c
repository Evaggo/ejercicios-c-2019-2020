#include <stdio.h>

//09. Write a program that asks for seconds and convert it into minutes, hours and days

int main ()
{
    float sec;
    float tmp;
    
    //Informa de lo que hace el programa
    printf("Si me dices una canditad de segundos, te lo paso a minutos, horas y dias\n");
    scanf("%f", &sec); //Pide los segundos
    getchar();
    
    //Limpia la pantalla
    system("cls");
    
    //Pasa de segundos a minutos
    tmp =  sec / 60;
    printf("El equivalente en minutos es: %f\n", tmp);
    
    //Pasa de segundos a horas
    tmp =  sec / 3600;
    printf("El equivalente en horas es: %f\n", tmp);
    
    //Pasa de segundos a dias
    tmp =  sec / (3600*24);
    printf("El equivalente en dias es: %f\n", tmp);
    
    
    getchar();
    return 0;
}