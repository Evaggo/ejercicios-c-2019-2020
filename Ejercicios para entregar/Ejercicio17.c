#include <stdio.h>
#include <stdlib.h>

int main ()
{
    time_t t; //Declara la variable tiempo
        
    // Pone la semilla del numero random en la hora en la que se ejecuta el programa
    srand((unsigned) time (&t));
    
    // Explica lo que hará el programa
    printf("Te voy a dar 5 numeros aleatorios que van del 0 al 150");
    getchar();
    
    //Bucle para poner 5 numeros randoms
    for (int cont = 0; cont < 5; cont++){
        printf("%d \n", rand()% 151); //Pone un numero aleatorio entre 0 y 150
        getchar();
    }
    
    printf("Y ya estaria!\n");
    getchar();
    return 0;
}