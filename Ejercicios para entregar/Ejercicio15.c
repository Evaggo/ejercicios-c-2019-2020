#include <stdio.h>

int main ()
{

    int num;
    int binario[50];
    int i = 0;
    int j;
    
    //Informa de lo que hace el programa
    printf("Dime un numero y te lo convierto en binario:");
    scanf("%i", &num);
    getchar();
    
    system("cls");  //Limpia la pantalla  
    printf("El numero decimal es: %d\n", num);
    
    // Calcula el numero binario
    while (num > 0){
     binario[i] = num % 2; //Se guarda el residuo (1 o 0)  
     num /= 2; //Divide el num entre 2 sucesivamente
     i++;     
    }
    
    printf("Su equivalente binario es: ");
    //El bucle pone el numero binario en el orden correcto
    for (j=i-1; j>=0;--j){
        printf("%d", binario[j]);
    }
    
    getchar();
    return 0;
}