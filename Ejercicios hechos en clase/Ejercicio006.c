#include <stdio.h>
#include <stdlib.h>

int main () {
   int numero1;
   int numero2;
   int tmp;
   int salida;
   
   printf("Dame 1 numero\n");
   scanf("%d",&numero1);
   getchar();
   
   printf("Dame 2 numero\n");
   scanf("%d",&numero2);
   getchar();
   
   tmp = numero2;
   salida = 0;
   
   printf("%d x %d = ",numero1,numero2);
   
   /*
   while(tmp>0){
      salida += numero1;
      printf("%d + ",numero1);
      tmp--;
   }
   
   printf("\b\b = %d\n",salida);
   */
   
   for(int a=numero2;a>0;a--){
      salida += numero1;
      printf("%d + ",numero1);
   }
   
   printf("\b\b = %d\n",salida);
   
   getchar();
   return(0);
}