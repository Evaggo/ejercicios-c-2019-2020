/*******************************************************************************************
*
*   raylib [core] example - Basic window
*
*   Welcome to raylib!
*
*   To test examples, just press F6 and execute raylib_compile_execute script
*   Note that compiled executable is placed in the same folder as .c file
*
*   You can find all basic examples on C:\raylib\raylib\examples folder or
*   raylib official webpage: www.raylib.com
*
*   Enjoy using raylib. :)
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2013-2016 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include "raylib.h"

int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1920;
    const int screenHeight = 1080;

    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
    
    Vector2 VLineStart = {(float)960, (float)0};
    Vector2 VLineEnd = {(float)960, (float)1080};
    
    Vector2 HLineStart = {(float)0, (float)540};
    Vector2 HLineEnd = {(float)1920, (float)540};

    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        if (IsKeyDown(KEY_W)) {
            VLineStart.x += 2.0f;
        }
        if (IsKeyDown(KEY_Q)) {
            VLineStart.x -= 2.0f;
        }
        
        if (IsKeyDown(KEY_RIGHT)) {
            VLineEnd.x += 2.0f;
        }
        if (IsKeyDown(KEY_LEFT)) {
            VLineEnd.x -= 2.0f;
        }
        
        if (IsKeyDown(KEY_A)){
            HLineStart.y += 2.0f;
        }
        if (IsKeyDown(KEY_S)){
            HLineStart.y -= 2.0f;
        }
        
        if (IsKeyDown(KEY_DOWN)) {
            HLineEnd.y += 2.0f;
        }
        if (IsKeyDown(KEY_UP)) {
            HLineEnd.y -= 2.0f;
        }
        
        BeginDrawing();

            ClearBackground(DARKGREEN);

           //DrawText("Esplai El Botó", 800, 400, 50, ORANGE);
           //DrawText("L'esplai millor", 815, 500, 50, ORANGE);
           DrawLineV(VLineStart, VLineEnd, ORANGE);
           DrawLineV(HLineStart, HLineEnd, ORANGE);
           
            


        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}