#include <stdio.h>
#include <stdlib.h>

int main () 
{
    long num1 = 0;
    long num2 = 1;
    long num3;
    long contador = 0;
    
    while (contador < 100) {
        num3 = num1 + num2;
        num1 = num2;
        num2 = num3;
        
        printf("%lu \n", num3);
        contador++;        
    }
    
    getchar();
    return 0;
}