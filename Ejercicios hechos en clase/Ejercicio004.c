#include <stdio.h>

int main ()
{
   int a = 0;
   int b = 0;
   int c = 0;
   int d = 0;
   
    //Informa de lo que hace el programa
    printf("Si me dices tres numeros, te digo cual es el mas grande!");
    getchar();
    system("cls");
    
   //Pide el primer numero
    printf("Dime un numero: ");
    scanf("%i",&a);
    getchar();
    
    //Pide el segundo numero
    system("cls"); 
    printf("Dime otro numero: ");
    scanf("%i", &b);
    getchar();
    
   //Pide el tercer numero
    system("cls");
    printf("Dime otro numero: ");
    scanf("%i", &c);
    getchar();
    
    //Informa de los numeros escogidos y pregunta si son los correctos
    system("cls");
    printf("Has escogido el %d, el %d y el %d \n", a, b, c);
    getchar();
    
    printf("Seguro que quieres estos numeros? \n");
    printf("En caso afirmativo, escribe 1 \n");
    printf("Si te has equivocado, escribe 2: ");
    scanf("%i", &d);
    getchar();
    
    system("cls");
    if (d == 1) {
        if ((a > b) && (a > c)){
            printf ("%d es mayor que %d i que %d", a, b, c);
            getchar();
        } else if ((b > a) && (b > c)) {
            printf ("%d es mayor que %d i que %d", b, a, c);
            getchar();
        } else if ((c > b) && (c > a)) {
            printf ("%d es mayor que %d i que %d", c, b, a);
            getchar();
        } else if ((a == b)&& (a == c)) {
            printf("%d es igual que %d i que %d", a, b, c);
            getchar();
        }
        
    } else if (d == 2) {
        system("cls");
        printf("Si no quieres saber que numero es mas grande... \n");
        printf("Que haces aqui? \n");
        getchar();        
    } else if ((d != 1) && (d != 2)){
        system("cls");
        printf("Eso ni siquiera era una opcion. \n");
        printf("Deja de hacerme perder el tiempo poniendo opciones inexistentes");
        getchar();
    }
   
   /*
    if ((a > b) &&(a > c)){
        printf ("%d es mayor que %d i que %d", a, b, c);
    } else if ((b > a) && (b > c)) {
        printf ("%d es mayor que %d i que %d", b, a, c);
    } else if ((c > b) && (c > a)) {
        printf ("%d es mayor que %d i que %d", c, b, a);
    } else if ((a == b)&& (a == c)) {
        printf("%d es igual que %d i que %d", a, b, c);
    }
             
    getchar();
    */
    return 0;
}