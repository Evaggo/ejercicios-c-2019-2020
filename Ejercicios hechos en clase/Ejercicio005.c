#include <stdio.h>

int main ()
{
    int a = 0;    
    int b = 0;
    int c = 0;
    
    //Informa de lo que hace el programa y pide un numero
    printf("Dime un nimero y te dare su tabla de multiplicar: ");
    scanf("%i", &a);
    getchar;
    
    //Informa de los numeros escogidos y pregunta si son los correctos
    system("cls");
    printf("Has escogido el %d \n", a);
    getchar();
    
    printf("Seguro que quieres este numero? \n");
    printf("En caso afirmativo, escribe 1 \n");
    printf("Si te has equivocado, escribe 2: ");
    scanf("%i", &c);
    getchar();
    
    if (c == 1) {
        system("cls");
        printf("%d x 0 = %d \n", a, b);
        getchar();
    
        b = a * 1;
        printf("%d x 1 = %d \n", a, b);
        getchar();
   
        b = a * 2;
        printf("%d x 2 = %d \n", a, b);
        getchar();
    
        b = a * 3;
        printf("%d x 3 = %d \n", a, b);
        getchar();
    
        b = a * 4;
        printf("%d x 4 = %d \n", a, b);
        getchar();
    
        b = a * 5;
        printf("%d x 5 = %d \n", a, b);
        getchar();
    
        b = a * 6;
        printf("%d x 6 = %d \n", a, b);
        getchar();
    
        b = a * 7;
        printf("%d x 7 = %d \n", a, b);
        getchar();
    
        b = a * 8;
        printf("%d x 8 = %d \n", a, b);
        getchar();
    
        b = a * 9;
        printf("%d x 9 = %d \n", a, b);
        getchar();
    
        b = a * 10;
        printf("%d x 10 = %d \n", a, b);
        getchar();
    } else if (c == 2) {
        system("cls");
        printf("Te has equivocado de numero? \n");
        printf("En caso afirmativo, escribe 1 \n");
        printf("Si quieres cerrar el programa, escribe cualquier otro numero: ");
        scanf("%i", &c);
        getchar();
        if (c == 1) {
            system("cls"); 
            printf ("Pues ahora te aguantas, por no saber escribir un simple numero.");
            getchar();
        } else {
            system("cls");
            printf("Hasta la proxima!");
            getchar();
            return 0;         
        }
            
    } else if ((c != 1) && (c != 2)){
        system("cls");
        printf("Eso ni siquiera era una opcion. \n");
        printf("Deja de hacerme perder el tiempo poniendo opciones inexistentes");
        getchar();
    }
    
    /*
    printf("%d x 0 = %d \n", a, b);
    getchar();
    
    b = a * 1;
    printf("%d x 1 = %d \n", a, b);
    getchar();
   
    b = a * 2;
    printf("%d x 2 = %d \n", a, b);
    getchar();
    
    b = a * 3;
    printf("%d x 3 = %d \n", a, b);
    getchar();
    
    b = a * 4;
    printf("%d x 4 = %d \n", a, b);
    getchar();
    
    b = a * 5;
    printf("%d x 5 = %d \n", a, b);
    getchar();
    
    b = a * 6;
    printf("%d x 6 = %d \n", a, b);
    getchar();
    
    b = a * 7;
    printf("%d x 7 = %d \n", a, b);
    getchar();
    
    b = a * 8;
    printf("%d x 8 = %d \n", a, b);
    getchar();
    
    b = a * 9;
    printf("%d x 9 = %d \n", a, b);
    getchar();
    
    b = a * 10;
    printf("%d x 10 = %d \n", a, b);
               
    getchar();
    */
    
    return 0;
}