#include <stdio.h>

int main ()
{
   int a = 0;
   int b = 0;
   int c = 0;
   
    //Informa de lo que hace el programa
    printf("Si me dices dos numeros, te digo cual es el mas grande!");
    getchar();
    system("cls"); //Para borrar lo que hay en pantalla
    
    //Pide el primer numero
    printf("Dime un numero: ");
    scanf("%i",&a);
    getchar();
    
    //Pide el segundo numero
    system("cls");
    printf("Dime otro numero: ");
    scanf("%i", &b);
    getchar();
    
    //Informa de los numeros escogidos y pregunta si son los correctos
    system("cls");
    printf("Has escogido el %d y el %d \n", a, b);
    getchar();
    
    printf("Seguro que quieres estos numeros? \n");
    printf("En caso afirmativo, escribe 1 \n");
    printf("Si te has equivocado, escribe 2: ");
    scanf("%i", &c);
    getchar();
    
    if (c == 1) {
        system("cls");
        if (a > b) {
        printf("%d es mayor que %d", a, b);
        } else if (a < b) {
       printf("%d es menor que %d", a, b);
        } else if (a == b){
       printf("%d es igual que %d", a ,b);
        }
        getchar();
    } else if (c == 2) {
        system("cls");
        printf("Si no quieres saber que numero es mas grande... \n");
        printf("Que haces aqui? \n");
        getchar();        
    } else if ((c != 1) && (c != 2)){
        system("cls");
        printf("Eso ni siquiera era una opcion. \n");
        printf("Deja de hacerme perder el tiempo poniendo opciones inexistentes");
        getchar();
    }
      
   /*
    if (a > b) {
       printf("%d es mayor que %d", a, b);
    } else if (a < b) {
       printf("%d es menor que %d", a, b);
    } else if (a == b){
       printf("%d es igual que %d", a ,b);
    }
   */
    return 0;
}