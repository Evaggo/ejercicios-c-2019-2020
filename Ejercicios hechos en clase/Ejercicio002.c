#include <stdio.h>

int main ()
{
    int a = 0;
    int b = 0;
    int c = 0;
    
    //Informa de lo que hace el programa 
    printf("Si me dices dos numeros, te los multiplico!");
    getchar();
    system("cls"); //Para borrar lo que hay en pantalla
    
    //Pide el primer numero
    printf("Dime un numero: ");
    scanf("%i",&a);
    getchar();
    
    //Pide el segundo numero
    system("cls"); 
    printf("Dime otro numero: ");
    scanf("%i", &b);
    getchar();
    
    //Informa de los numeros escogidos y pregunta si son los correctos
    system("cls");
    printf("Has escogido el %d y el %d \n", a, b);
    getchar();
    
    printf("Seguro que quieres estos numeros? \n");
    printf("En caso afirmativo, escribe 1 \n");
    printf("Si te has equivocado, escribe 2: ");
    scanf("%i", &c);
    getchar();
    
    if (c == 1) {
        system("cls");
        int d = a*b;
        printf("%d multiplicado por %d es igual a %d", a, b, d);
        getchar();
    } else if (c == 2) {
        system("cls");
        printf("Si no quieres multiplicar estos numeros... \n");
        printf("Que haces aqui? \n");
        getchar();        
    } else if ((c != 1) && (c != 2)){
        system("cls");
        printf("Eso ni siquiera era una opcion. \n");
        printf("Deja de hacerme perder el tiempo poniendo opciones inexistentes");
        getchar();
    }
    
    return 0;
}