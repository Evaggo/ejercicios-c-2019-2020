#include <stdio.h>

int main ()
{
    int a = 0;
    
    printf("Dime un nimero y te dare su tabla de multiplicar: ");
    scanf("%i", &a);
    getchar();
    
    for (int i = 0; i <= 10; i++) {
        printf("%d x %d = %d \n", a, i, a*i);
    }
    getchar();
    return 0;
}